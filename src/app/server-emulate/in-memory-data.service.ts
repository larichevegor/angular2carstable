import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        const table = [
            {
                id: 11,
                carBrand: 'lada',
                mileage: '100 0023230 km',
                service: 'Замена масла',
                visitTime: [
                    {
                        time: '15:00',
                        date: '2017-12-20'
                    },
                    {
                        time: '17:00',
                        date: '2017-12-21'
                    },
                ],
                servicePrice: 3200,
                partsPrice: 1500,
            },
            {
                id: 12,
                carBrand: 'lada',
                mileage: '100 0012312310 km',
                service: 'Замена масла',
                visitTime: [
                    {
                        time: '12:00',
                        date: '2017-12-23'
                    },
                    {
                        time: '10:00',
                        date: '2017-12-31'
                    },
                ],
                servicePrice: 3200,
                partsPrice: 1800,
            },
            
        ];
        return { table };
    }
}