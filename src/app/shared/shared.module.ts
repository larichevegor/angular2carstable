import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { CommonModule } from '@angular/common';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCardModule} from '@angular/material/card';
import {MatRadioModule} from '@angular/material/radio';
import {MatNativeDateModule} from '@angular/material';







const reexport = [
    MatCheckboxModule,
    MatButtonModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatTableModule,
    CommonModule,
    MatMenuModule,
    MatIconModule,
    MatDialogModule,
    MatCardModule,
    MatNativeDateModule,
    MatInputModule,
    MatDatepickerModule,
    MatRadioModule
];

const declarations = [

];

@NgModule({
    imports: [
        ...reexport
    ],
    declarations: [
        ...declarations,
    ],
    exports: [
        ...reexport,
        ...declarations,
    ],
})

export class SharedModule { }