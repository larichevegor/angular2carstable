import { Component, OnInit } from '@angular/core';
import { TableService } from '../../core/services/table.service';
import { Table } from '../../models/table';
import { DatePipe } from '@angular/common';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { RewriteTableModalComponent } from '../rewrite-table-modal/rewrite-table-modal.component';
import { SendTableModalComponent } from '../send-table-modal/send-table-modal.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  table: Table[];
  error: any;
  dataSource: MatTableDataSource<Table>;

  constructor(private tableService: TableService,
    public dialog: MatDialog) { }

  displayedColumns = ['id', 'carBrand', 'mileage', 'service', 'visitTime', 'Actions'];

  ngOnInit() {
    this.getTable();
  }

  getTable(): void {
    this.tableService
      .getTable()
      .then(table => {
        this.table = table;
        console.log(this.table)
        this.dataSource = new MatTableDataSource<Table>(this.table);
      })
      .catch(error => this.error = error);
  }

  public openRewriteWindow(row) {
    let dialogRef = this.dialog.open(RewriteTableModalComponent, { data: row})
    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return
      }
      let index = this.table.findIndex(x => x.id == result.id);
      console.log('x',index)
      this.table[index] = result;
      console.log(this.table)
      console.log('The dialog was closed', this.table);
      this.dataSource = new MatTableDataSource<Table>(this.table);
      
    });
    
  }

  public openSendWindow(row) {
    let dialogRef = this.dialog.open(SendTableModalComponent, { data: row});
    dialogRef.afterClosed().subscribe(result => {
      this.table = result
      console.log('The dialog was closed', result);
    });
  }

}
