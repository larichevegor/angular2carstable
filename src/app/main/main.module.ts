import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { RewriteTableModalComponent } from './rewrite-table-modal/rewrite-table-modal.component';
import { SendTableModalComponent } from './send-table-modal/send-table-modal.component';

import { TableComponent } from './table/table.component';
import { Routing } from './main.routing';



@NgModule({
    imports: [
        Routing,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    declarations: [
        TableComponent,
        RewriteTableModalComponent,
        SendTableModalComponent
    ],
    entryComponents: [RewriteTableModalComponent,SendTableModalComponent]
})

export class MainModule { }