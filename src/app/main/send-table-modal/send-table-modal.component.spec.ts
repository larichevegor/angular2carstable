import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendTableModalComponent } from './send-table-modal.component';

describe('SendTableModalComponent', () => {
  let component: SendTableModalComponent;
  let fixture: ComponentFixture<SendTableModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendTableModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendTableModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
