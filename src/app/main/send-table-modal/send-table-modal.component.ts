import { Component, OnInit, ViewEncapsulation, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { inject } from '@angular/core/testing';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { patternValidator } from '../../core/services/validation.service';
import { element } from 'protractor';
import { TableService } from '../../core/services/table.service';
import { Observable, Subscription } from 'rxjs/Rx';


@Component({
  selector: 'app-send-table-modal',
  templateUrl: './send-table-modal.component.html',
  styleUrls: ['./send-table-modal.component.scss']
})
export class SendTableModalComponent implements OnInit {
  discounts: any;
  totalPrice: number;
  commission: number;
  ticks = 14 * 60;
  minutesDisplay: number = 0;
  secondsDisplay: number = 0;
  sub: Subscription;


  constructor(private matDialogRef: MatDialogRef<SendTableModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private tableService: TableService) { }

  ngOnInit() {
    this.startTimer();
    this.discounts = [
      { forShow: '0%', forCount: 1 },
      { forShow: '5%', forCount: 0.95 },
      { forShow: '10%', forCount: 0.9 },
      { forShow: '15%', forCount: 0.85 },
      { forShow: '20%', forCount: 0.8 },
    ]
    this.countPrice(this.discounts[0].forCount)
    console.log(this.data)
  }

  countPrice(discount) {
    this.commission = this.data.servicePrice * 0.1
    this.totalPrice = this.data.servicePrice * discount + this.data.partsPrice - this.commission;
  }


  toggleClick(clickedItem: any): void {
     console.log(clickedItem)
    for (let discount of this.discounts) {
      discount.isClicked = false;
    }
    this.countPrice(clickedItem.forCount)
    
    clickedItem.isClicked = true;
  }








  private startTimer() {

    let timer = Observable.timer(1, 1000);
    this.sub = timer.subscribe(
      t => {
        if (this.ticks == 1) {
          return this.matDialogRef.close()
        }
        this.ticks = this.ticks - 1;
        this.secondsDisplay = this.getSeconds(this.ticks);
        this.minutesDisplay = this.getMinutes(this.ticks);
      }
    );
  }

  private getSeconds(ticks: number) {
    return this.pad(ticks % 60);
  }

  private getMinutes(ticks: number) {
    return this.pad((Math.floor(ticks / 60)) % 60);
  }

  private getHours(ticks: number) {
    return this.pad(Math.floor((ticks / 60) / 60));
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }



}
