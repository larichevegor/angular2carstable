import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TableComponent } from './table/table.component';

const routes: Routes = [
  { path: '', component: TableComponent }
];

export const Routing: ModuleWithProviders = RouterModule.forChild(routes);