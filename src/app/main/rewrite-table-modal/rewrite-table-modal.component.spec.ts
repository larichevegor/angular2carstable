import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewriteTableModalComponent } from './rewrite-table-modal.component';

describe('RewriteTableModalComponent', () => {
  let component: RewriteTableModalComponent;
  let fixture: ComponentFixture<RewriteTableModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewriteTableModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewriteTableModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
