import { Component, OnInit, ViewEncapsulation, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { inject } from '@angular/core/testing';
import { FormGroup, FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormArray } from '@angular/forms';
import { patternValidator } from '../../core/services/validation.service';
import { element } from 'protractor';
import { TableService } from '../../core/services/table.service';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid);
    const invalidParent = !!(control && control.parent && control.parent.invalid);

    return (invalidCtrl || invalidParent);
  }
}


@Component({
  selector: 'app-rewrite-table-modal',
  templateUrl: './rewrite-table-modal.component.html',
  styleUrls: ['./rewrite-table-modal.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class RewriteTableModalComponent implements OnInit {
  form: FormGroup;
  matcher = new MyErrorStateMatcher();
  date: FormGroup;
  formData: any;
  carBrand: string;
  mileage: string;
  service: string;
  visitTime: Array<Object>;
  value: Date = new Date();



  constructor(private matDialogRef: MatDialogRef<RewriteTableModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private tableService: TableService,
    private _fb: FormBuilder) {

  }

  ngOnInit() {
    console.log(this.data)
    this.form = this._fb.group({
      id: [this.data.id, [Validators.required]],
      carBrand: [this.data.carBrand, [Validators.required]],
      mileage: [this.data.mileage, [
        Validators.required,
      ]],
      service: [this.data.service, [
        Validators.required,
      ]],
      visitTime: this._fb.array([])
    },
    );
    this.initDate()
  }


  initDate() {
    const control = <FormArray>this.form.controls['visitTime'];
    this.data.visitTime.forEach(element => {
      control.push(this.Date(element));
    });
  }

  addNewDate() {
    const control = <FormArray>this.form.controls['visitTime'];
    control.push(this._fb.group({
      date: ['', [Validators.required]],
      time: ['', [Validators.required]],
    }, {
        validator: (formGroup: FormGroup) => {
          return this.validateData(formGroup);
        }
      }))
  }

  Date(element) {
    return this._fb.group({
      date: [element.date, [Validators.required]],
      time: [element.time, [Validators.required]],
    },
      {
        validator: (formGroup: FormGroup) => {
          return this.validateData(formGroup);
        }
      });
  }

  deleteDate(index) {
    const control = <FormArray>this.form.controls['visitTime'];
    control.removeAt(index)
  }

  show(x) {
    console.log(x)
  }




  private validateData(formGroup: FormGroup) {
    let date = new Date(formGroup.controls["date"].value)
    let time = formGroup.controls["time"].value
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    let currentMonth = currentDate.getMonth();
    let currentDay = currentDate.getDate();
    let currentHour = currentDate.getHours();
    let currentMinutes = currentDate.getTime();

    time = time.split(':')[0]

    // console.log(time, currentHour);
    console.log(date.getFullYear(), currentYear);

    if (date.getFullYear() < currentYear) {
      return { dateIsNotValid: 'Введите валидную дату' }
    }

    if (date.getFullYear() > currentYear) {
      return null
    }

    if (date.getMonth() < currentMonth) {
      return { dateIsNotValid: 'Введите валидную дату' }
    }

    if (date.getMonth() > currentMonth) {
      return null
    }

    if (date.getDate() < currentDay) {
      return { dateIsNotValid: 'Введите валидную дату' }
    }


    if (date.getDate() > currentDay) {
      return null
    }

    if (time > currentHour) {
      return null;
    }

    return { timeIsNotValid: 'Введите валидное время' }

  }



  save(event): void {

    this.tableService
      .saveTable(this.form.value)
      .then(table => {
        console.log('suicide', table)
        this.matDialogRef.close(table);

      })
      .catch(error => console.log(error));


  }


  // dateValidation(dates: FormGroup) {


  //   let currentData = new Date()
  //   let testYear = currentData.getFullYear()
  //   let testYear = currentData.getFullYear()
  //   let testYear = currentData.getFullYear()


  //   let year = date.value.getYear()

  //   const exactMatch = input.root.controls.password === input.value;
  //   return exactMatch ? null : { mismatchedPassword: true };
  // }



}
