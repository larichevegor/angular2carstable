import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import { catchError, map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';

import { Table } from '../../models/table';

@Injectable()
export class TableService {
  private tableUrl = 'api/table'

  constructor(private http: Http) { }

  getTable(): Promise<Array<Table>> {
    return this.http
    .get(this.tableUrl)
    .toPromise()
    .then((response) => {
      return response.json() as Table[];
    })
    .catch(this.handleError);
  }

  saveTable(table: Table): Promise<Table> {
    if (table.id) {
      return this.put(table);
    }
  }


  private put(table: Table): Promise<Table> {
   console.log(table.id)
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const url = `${this.tableUrl}/${table.id}`;

    return this.http
      .put(url, JSON.stringify(table), { headers: headers })
      .toPromise()
      .then(() => table)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
