import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    { path: 'table', loadChildren: './main/main.module#MainModule' },];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);