import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './server-emulate/in-memory-data.service';

import { AppComponent } from './app.component';
// import { RewriteTableModalComponent } from './rewrite-table-modal/rewrite-table-modal.component';
// import { SendTableModalComponent } from './send-table-modal/send-table-modal.component';
import { Routing } from './app.routing';
import {DpDatePickerModule} from 'ng2-date-picker';





@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    Routing,
    SharedModule,
    CoreModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DpDatePickerModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, { delay: 600 })
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { }
